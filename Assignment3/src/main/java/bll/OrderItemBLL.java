package bll;

import java.util.List;

import dao.OrderItemDAO;
import model.OrderItem;

public class OrderItemBLL {
	public OrderItemDAO orderItemDAO;
	
	/**
	 * Constructorul clasei OrderItemBLL, unde este initializat orderItemDAO
	 */
	public OrderItemBLL() {
		orderItemDAO = new OrderItemDAO();
	}
	
	/**
	 * Se apeleaza metoda selectAll din orderItemDAO,apeland metoda din OrderItemDAO
	 * @return O lista cu toate obiectele de tip orderItem din baza de date
	 */
	public List<OrderItem> selectOrderItems() {
		return orderItemDAO.selectAll();
	}
	
	/**
	 * Gaseste orderItem-ul cu id-ul si produsul dorit,apeland metoda din OrderItemDAO
	 * @param id - id-ul ce trebuie sa il aiba obiectul
	 * @param name - numele produsului din orderItem
	 * @return Elementul din baza de date corespunzator
	 */
	public OrderItem selectByIdPlusClient(int id, String name) {
		return orderItemDAO.findByIdPlusClient(id, name);
	}
	/**
	 * Gaseste orderItem-ul cu id-ul dorit, apeland metoda din OrderItemDAO
	 * @param id - id-ul elementului ce trebuie cautat
	 * @return Elementul din baza de data corespunzator
	 */
	public List<OrderItem> selectById(int id) {
		return orderItemDAO.findById(id);
	}
	
	/**
	 * Insereaza un orderItem, apeland metoda din OrderItemDAO
	 * @param orderItem - obiectul care se insereaza
	 */
	public void insertOrderItem(OrderItem orderItem) {
		orderItem = orderItemDAO.insert(orderItem);
	}
	/**
	 * Stegre un orderItem, apeland metoda din OrderItemDAO
	 * @param orderItem - obiectul ce este sters
	 */
	public void deleteOrderItem(OrderItem orderItem) {
		orderItemDAO.delete(orderItem);
	}
	
	/**
	 * Actualizeaza un orderItem, apeland metoda din OrderItemDAO
	 * @param orderItem - obiectul ce este actualizat
	 * @param field - campul ce este actualizat
	 */
	public void updateOrderItem(OrderItem orderItem, String field) {
		orderItemDAO.updateTotal(orderItem, field);
	}
}
