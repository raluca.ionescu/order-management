package bll;

import java.util.List;

import dao.ClientDAO;
import model.Client;

public class ClientBLL {

	public ClientDAO clientDAO;
	
	/**
	 * Constructorul clasei clientBLL, unde este initializat ClientDAO
	 */
	public ClientBLL() {
		clientDAO = new ClientDAO();
	}
	
	/**
	 * Se apeleaza metoda  selectAll din clientDAO
	 * @return O lista cu toti clientii din baza de date
	 */
	public List<Client> selectClients() {
		return clientDAO.selectAll();
	}
	
	/**
	 * Se apeleaza metoda  findByName  din clientDAO
	 * @param name - numele in fucntie de care se face cautarea in baza de date
	 * @return Un client din baza de date, care are numele dorit 
	 */
	public Client selectByName(String name) {
		return clientDAO.findByName(name);
	}
	
	/**
	 * Se apeleaza metoda  findById  din clasa clientDAO
	 * @param id - id-ul in functie de care se face cautarea
	 * @return Un client din baza de date cu id-ul dorit
	 */
	public Client selectById(int id) {
		return clientDAO.findById(id);
	}
	
	/**
	 * Se insereaza un client in baza de date
	 * Se apeleaza metoda  insert din clasa ClientDAO
	 * @param client - clientul ce se vrea inserat
	 */
	public void insertClient(Client client) {
		client = clientDAO.insert(client);
	}
	
	/**
	 * Se sterge un client din baza de date
	 * Se apeleaza functia delete din clasa CleintDAO
	 * @param client - clientul ce va fi sters
	 */
	public void deleteClient(Client client) {
		clientDAO.delete(client);
	}
	
	/**
	 * Se face un update asupra unui client
	 * @param client - clientul ce se vrea actualizat
	 * @param field - campul ce urmeaza sa fie actualizat
	 */
	public void updateClient(Client client, String field) {
		client = clientDAO.update(client, field);
	}
	
}
