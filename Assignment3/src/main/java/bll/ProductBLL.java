package bll;

import java.util.List;

import dao.ProductDAO;
import model.Product;

public class ProductBLL {
	public ProductDAO productDAO;
	/**
	 * Constructorul clasei ProdcutBLL , unde este initializat productDAO
	 */
	public ProductBLL() {
		productDAO = new ProductDAO();
	}
	
	/**
	 * Apeleaza metoda selectAll din ProductDAO
	 * @return Produsele din baza de date
	 */
	public List<Product> selectProducts() {
		return productDAO.selectAll();
	}
	
	/**
	 * Apeleaza metoda findByName din ProductDAO
	 * @param name - numele in functie de care se face selectia
	 * @return Produsul din baza de date
	 */
	public Product selectByName(String name) {
		return productDAO.findByName(name);
	}
	
	/**
	 * Insereaza un produs in baza de date, apeland metoda insert din ProductDAO
	 * @param product - produsul care va fi inserat
	 */
	public void insertProduct(Product product) {
		product = productDAO.insert(product);
	}
	/**
	 * Sterge un produs din baza de date, apeland metoda delete din ProductDAO
	 * @param product - produsul ce va fi sters
	 */
	public void deleteProduct(Product product) {
		productDAO.delete(product);
	}
	/**
	 * Actualizeaza un produs din baza de date, apeland metoda update din ProductDAO
	 * @param product - produsul ce va fi actualizat
	 * @param field - campul produsului ce va fi actualizat
	 */
	public void updateProduct(Product product, String field) {
		product = productDAO.update(product, field);
	}
}
