package bll;

import java.util.List;

import dao.OrderDAO;
import model.Order;

public class OrderBLL {
	public OrderDAO orderDAO;
	
	/**
	 * Constructorul clasei OrderBLL, unde este initializat orderDAO
	 */
	public OrderBLL() {
		orderDAO = new OrderDAO();
	}
	
	
	/**
	 * Se apeleaza metoda selectAll din orderDAO
	 * @return O lista cu toate comenzile din baza de date
	 */
	public List<Order> selectOrders() {
		return orderDAO.selectAll();
	}
	
	/**
	 * Se apeleaza metoda findById din clasa OrderDAO
	 * @param id - id-ul in functie de care se face cautarea
	 * @return Un order din baza de date cu id-ul dorit
	 */
	public Order selectById(int id) {
		return orderDAO.findById(id);
	}
	/**
	 * Se apeleaza metoda findByClient din clasa OrderDAO
	 * @param id - id-ul clientului in functie de care se face selecti
	 * @return Un order din baza de date cu id-ul dorit
	 */
	public Order selectByClientId(int id) {
		return orderDAO.findByClient(id);
	}
	/**
	 * Se insereaza o comanda in baza de date
	 * Se apeleaza metoda insert din clasa OrderDAO
	 * @param order - comanda ce se vrea inserat
	 */
	public void insertOrder(Order order) {
		order = orderDAO.insert(order);
	}
	
	/**
	 * Se sterge o comanda din baza de date
	 * Se apeleaza metoda delete din clasa OrderDAO
	 * @param order - comanda ce va fi sters
	 */
	public void deleteOrder(Order order) {
		orderDAO.delete(order);
	}
	

	/**
	 * Se face un update asupra unei comanezi
	 * @param order - comanda ce se vrea actualizat
	 * @param field - campul ce urmeaza sa fie actualizat
	 */
	public void updateOrder(Order order, String field) {
		orderDAO.updateIdOrder(order, field);
	}
}
