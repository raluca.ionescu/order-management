package start;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.OrderItemBLL;
import bll.ProductBLL;
import model.Product;
import presentation.FileParser;

public class Main {

	public static void main(String[] args) {

		 ClientBLL client = new ClientBLL();
		 OrderBLL order = new OrderBLL();
		 ProductBLL product=new ProductBLL();
		 OrderItemBLL orderItem = new OrderItemBLL();
		 
		 FileParser filep = new FileParser(args[0], client, order,orderItem, product);
		 filep.readCommands();
		 filep.executeCommands();
	}
		
}
