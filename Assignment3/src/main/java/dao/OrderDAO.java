package dao;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import connection.ConnectionFactory;
import model.Order;

public class OrderDAO extends AbstractDAO<Order> {

	public OrderDAO() {
		super(Order.class);
	}
	
	/**
	 * Gaseste in baza de date comanda (Order) in functie de un idOrder
	 * @param id - id-ul in functie de care se face cautarea
	 * @return Un obiect de tipul Order rezultat
	 */
	public Order findById(int id){
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		String query = selectQuery("idOrder");
		
		connection = ConnectionFactory.getConnection();
		try {
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			result = statement.executeQuery();
			if(createObjects(result).size()!=0)
			return createObjects(result).get(0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(result);
			ConnectionFactory.close(connection);
		}
     
		return null;
	}
	
	/**
	 *Gaseste in baza de date comanda (Order) in functie de un idClient
	 * @param id - id-ul clientului in functie de care se face cautarea
	 * @return Un obiect de tipul Order rezultat
	 */
	public Order findByClient(int id){

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		String query = selectQuery("idClient");
		
		connection = ConnectionFactory.getConnection();
		try {
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			result = statement.executeQuery();
			List<Order> obj = createObjects(result);
			if(obj.size()!= 0)
			  return obj.get(0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(result);
			ConnectionFactory.close(connection);
		}
     
		return null;
	}
	

}
