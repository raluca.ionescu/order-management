package dao;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import connection.ConnectionFactory;
import model.OrderItem;

public class OrderItemDAO extends AbstractDAO<OrderItem>{

	public OrderItemDAO() {
		super(OrderItem.class);
	}
	/**
	 * Gaseste in baza de date elemente orderItem in functie de un idOrder
	 * @param id - id-ul in functie de care se face cautarea
	 * @return O lista de obiecte de tipul OrderItem
	 */
	public List<OrderItem> findById(int id){
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		String query = selectQuery("idOrder");
		
		connection = ConnectionFactory.getConnection();
		try {
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			result = statement.executeQuery();
			
			return createObjects(result);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(result);
			ConnectionFactory.close(connection);
		}
     
		return null;
	}
	
	/**
	 * Gaseste in baza de date elemente orderItem in functie de un idOrder
	 * si un nume al clientului 
	 * @param id - id-ul in functie de care se face cautarea
	 * @param name - numele clientului pentru care se gasesc elementele
	 * @return O lista de obiecte de tipul OrderItem
	 */
	public OrderItem findByIdPlusClient(int id, String name){
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		String query = selectQuery("idOrder");
		
		connection = ConnectionFactory.getConnection();
		try {
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			result = statement.executeQuery();
			List<OrderItem> list = createObjects(result);
			
			if(list.size()!=0) {
				for(OrderItem o: list) { // the orderItems with the idOrder = id
					
					if(o.getNameProduct().equals(name)) {
						//System.out.println(o.toString());
						return o;
					}
						
  				}
			}			
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(result);
			ConnectionFactory.close(connection);
		}
     
		return null;
	}
	
	  /**
	   * Se face un update in baza de date la un obiect dat ca parametru
	   * Obiectul trebuie sa existe in baza de date.
	   * Cautarea se face in functie de numele produsului.(pentru obiectele de tip OrderItem
	   * care au acelasi idOrder, numele produsului le diferentiaza).
	   * @param orderItem - elementul ce-l actualizam
	   * @param fieldToUpdate - campul pe care il actualizam
	   * @return Acelasi obiect actualizat
	   */
	
	public OrderItem updateTotal(OrderItem orderItem, String fieldToUpdate) {
		
		  Connection connection = ConnectionFactory.getConnection();
		  String query = updateQuery(fieldToUpdate, "nameProduct");
		  PreparedStatement statement = null;
		
		  try {
			  statement =connection.prepareStatement(query);
			 for(Field field : orderItem.getClass().getDeclaredFields()) {
				 field.setAccessible(true);
				 if(field.getName().equals(fieldToUpdate)) {
					 Object value = field.get(orderItem);
					 statement.setObject(1, value);
				 }
				 else if(field.getName().equals("nameProduct")) {
					 Object value = field.get(orderItem);
					 statement.setObject(2, value);
				 }
				 
			 }
			 
			 statement.executeUpdate();		  
		  } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}finally {
			
			ConnectionFactory.close(statement);;
			ConnectionFactory.close(connection);
		}
		  
		return orderItem;
	}
}

