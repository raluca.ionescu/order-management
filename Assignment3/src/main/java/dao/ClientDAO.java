package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import connection.ConnectionFactory;
import model.Client;

public class ClientDAO extends AbstractDAO<Client>{

	public ClientDAO() {
		super(Client.class);
	}
	
	/**
	 * Gaseste in baza de date toti clientii care au un anumit id
	 * @param id - id-ul in functie de care se face cautarea/selectia
	 * @return Clientul gasit
	 */
	public Client findById(int id){
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		String query = selectQuery("idClient");
		
		connection = ConnectionFactory.getConnection();
		try {
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			result = statement.executeQuery();
			List<Client> list = createObjects(result);
			if(list.size()!=0)
			return list.get(0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(result);
			ConnectionFactory.close(connection);
		}
     
		return null;
	}
}
