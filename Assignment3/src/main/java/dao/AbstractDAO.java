package dao;

import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import connection.ConnectionFactory;
import model.Order;

public class AbstractDAO<T> {

	  private final Class<?> type;
	  
	  
	  public AbstractDAO() {
		  this.type = (Class<?>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	  }
	  
	  public AbstractDAO(Class<?> type) {
		  this.type=type;
	  }
	  
	  /**
	   * Creeaza o interogare SQL de forma <br>
	   * <b>SELECT</b> * <b>FROM</b> table<br>
	   * <b>WHERE</b> field = ? <br>
	   * @param field - numele field-ului folosit in clauza WHERE 
	   * @return Un string care contine aceasta interogare
	   */
	  protected String selectQuery(String field) {
		  StringBuilder sb = new StringBuilder();
		  sb.append("SELECT ");
		  sb.append(" * ");
		  sb.append(" FROM ");
		  sb.append("`" + type.getSimpleName() + "`");
		  sb.append(" WHERE " + field + "=?");
		  
		  return sb.toString();
		  
	  }
	  
	  /**
	   * Creeaza o interogare SQL de forma <br>
	   * <b>INSERT INTO</b> table<br>
	   * <b>VALUES(....)</b> <br>
	   * @return  Un string care contine aceasta interogare
	   */
	  protected String insertQuery() {
		  StringBuilder sb = new StringBuilder();
		  
		  sb.append("INSERT INTO ");
		  sb.append("`" + type.getSimpleName() + "`");
		  
		  sb.append(" VALUES ( ");
		  
		  for(Field field: type.getDeclaredFields()) {
			 
			 sb.append(" ? ,");
			 		  
		  }
		  
		  sb.deleteCharAt(sb.length()-1);
		  sb.append(")");
		  
		  return sb.toString();  
	  }
	  
	  /**
	   * Creeaza o interogare SQL de forma <br>
	   * <b>UPDATE</b>  table<br>
	   * <b>SET</b> field = ? <br>
	   * <b>WHERE </b> field = ? <br>
	   * 
	   * @param fieldToUpdate- campul caruia vrem sa ii face update
	   * @param fieldWhere - campul care este verificat in clauza where
	   * @return  un string care contine interogare UPDATE
	   */
	  protected String updateQuery(String filedToUpdate, String filedWhere) {
		  
		  StringBuilder sb = new StringBuilder();
		  sb.append("UPDATE ");
		  sb.append("`" + type.getSimpleName() + "`");
		  sb.append(" SET " + filedToUpdate + " = ? WHERE "+ filedWhere + " = ?");
		  
		  return sb.toString();
		  	  
	  }
	  
		/**
		 * Creeaza o interogare SQL de forma <br>
		 * <b>DELETE FROM</b> table  <br>
		 * <b>WHERE</b> filed  = ?  <br>
		 * @param fieldWhere - numele campului ce se verfica in clauza WHERE
		 * @return Un string ce contine interogarea DELETE
		 */
	  protected String deleteQuery(String fieldWhere) {
		  
		  StringBuilder sb = new StringBuilder();  
		  sb.append("DELETE FROM ");
		  sb.append("`" + type.getSimpleName() + "`");
		  sb.append(" WHERE " + fieldWhere + " = ?");
		  
		  return sb.toString();
		  
	  }
	  
	  /**
		 * Metoda foloseste tehnica "reflection" pentru a converti
		 * {@link ResultSet} intr-o lista de obiecte 
		 * @param resultSet - ResultSet ce este convertit
		 * @return O lista de obiecte corespunzatoare tipului type
		 */
	  public List<T> createObjects(ResultSet resultSet){
		  List<T> list = new ArrayList<T>();
		  
			  try {
				while(resultSet.next()) {
					  @SuppressWarnings("unchecked")
					T instance = (T) type.newInstance();
					  for(Field field: type.getDeclaredFields()) {
						  Object value = resultSet.getObject(field.getName());
						  PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
						  Method method = propertyDescriptor.getWriteMethod();
						  method.invoke(instance,  value);
					  }
					  list.add(instance);
				  }
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
		  return list;
		  
		  
	  }
	  
	  /**
	   * Gaseste in baza de date si returneaza elementele
	   * corespunzatoare tipului de clasa in functie de un nume
	   * @param name - numele in functie de care se face cautare
	   * @return Obiectul rezultat daca exista, null in caz contrar
	   */
	 
	  public T findByName(String name) {
		  Connection connection = null;
		  PreparedStatement statement = null;
		  ResultSet resultSet = null;
		  String query = selectQuery("name");
		  
			  connection = ConnectionFactory.getConnection();
			  try {
				statement = connection.prepareStatement(query);
			
			  statement.setString(1, name);
			  resultSet = statement.executeQuery();
			  
			  List<T> elem = createObjects(resultSet);
			  if(elem.size()!=0)
				  return elem.get(0);
			  } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally {
					ConnectionFactory.close(resultSet);
					ConnectionFactory.close(statement);
					ConnectionFactory.close(connection);
				}
		  
			  return null;
	  }
	  
	  /**
	   * Insereaza in baza de date un obiect
	   * apeland query-ul returnat de {@link insertQuery}
	   * Metoda face un update la id-ul obiectului
	   * @param obj - elementul ce va fi inserat
	   * @return Acelasi obiect inserat, cu id-ul generat aici
	   */
	  public T insert(T obj) {
		  int i = 1;
		  Connection connection = null;
		  PreparedStatement statement = null;
		  String query = insertQuery();
		  
		  connection = ConnectionFactory.getConnection();
		  try {
			statement = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
		
		  for(Field field: type.getDeclaredFields()) {
			  
			  field.setAccessible(true);
			  Object value = field.get(obj);
				 
			  statement.setObject(i,value);
			  i++;	 
		  }
		  
		  statement.executeUpdate();
		  ResultSet pKey = statement.getGeneratedKeys();
	      
		  int newId = 0;
		  if( pKey.next()) {
			  newId = pKey.getInt(1);
		  }
		  
		  StringBuilder idName = new StringBuilder();
		  String name = type.getSimpleName();
		  idName.append("id");
		  if(name.equals("OrderItem")) {
			  idName.append("Order");
		  }
		  else
			  idName.append(name);
		  
		  PropertyDescriptor propertyDescriptor;
		try {
			propertyDescriptor = new PropertyDescriptor(idName.toString(), type);
			Method method = propertyDescriptor.getWriteMethod();
			method.invoke(obj, newId);
		
		} catch (IntrospectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
	
		  } catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			 
		  return obj;  
	  }
	  
	  /**
	   * Se face un update in baza de date la un obiect dat ca parametru
	   * Obiectul trebuie sa existe in baza de date
	   * @param t - elementul ce-l actualizam
	   * @param fieldToUpdate - campul pe care il actualizam
	   * @return Acelasi obiect actualizat
	   */
	  public T update(T t, String fieldToUpdate) {
		  
		  Connection connection = ConnectionFactory.getConnection();
		  String query = updateQuery(fieldToUpdate, "name");
		  PreparedStatement statement = null;
		
		  try {
			  statement =connection.prepareStatement(query);
			 for(Field field : type.getDeclaredFields()) {
				 field.setAccessible(true);
				 if(field.getName().equals(fieldToUpdate)) {
					 Object value = field.get(t);
					 statement.setObject(1, value);
				 }
				 else if(field.getName().equals("name")) {
					 Object value = field.get(t);
					 statement.setObject(2, value);
				 }
				 
			 }
			 
			 statement.executeUpdate();		  
		  } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}finally {
			
			ConnectionFactory.close(statement);;
			ConnectionFactory.close(connection);
		}
		  
			 return t;
	  }
	  
	  /**
	   * Se face un update in baza de date la un obiect dat ca parametru
	   * Obiectul trebuie sa existe in baza de date. Selectia se face in functie de idOrder
	   * @param t - elementul ce-l actualizam
	   * @param fieldToUpdate - campul pe care il actualizam
	   * @return Acelasi obiect actualizat
	   */
	  public T updateIdOrder(T t, String fieldToUpdate) {
		  Connection connection = ConnectionFactory.getConnection();
		  String query = updateQuery(fieldToUpdate, "idOrder");
		  PreparedStatement statement = null;
		
		  try {
			  statement =connection.prepareStatement(query);
			 for(Field field : t.getClass().getDeclaredFields()) {
				 field.setAccessible(true);
				 if(field.getName().equals(fieldToUpdate)) {
					 Object value = field.get(t);
					 statement.setObject(1, value);
				 }
				 else if(field.getName().equals("idOrder")) {
					 Object value = field.get(t);
					 statement.setObject(2, value);
				 }
				 
			 }
			 
			 statement.executeUpdate();		  
		  } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}finally {
			
			ConnectionFactory.close(statement);;
			ConnectionFactory.close(connection);
		}
		  
			 return t;
	}
	  /**
	   * Sterge din baza de date un element.
	   * Acesta trebuie sa existe in baza de date
	   * @param t - obiectul ce este sters
	   */
	  public void delete(T t) {
		  Connection connection = null;
		  PreparedStatement statement = null;
		  String query = deleteQuery("name");
		  
		  
			  connection = ConnectionFactory.getConnection();
			  try {
				statement = connection.prepareStatement(query);
				  PropertyDescriptor  name = new PropertyDescriptor("name", type);
				  Method getName = name.getReadMethod();
				  Object n = getName.invoke(t);
				  statement.setObject(1,n);
				  statement.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				ConnectionFactory.close(statement);;
				ConnectionFactory.close(connection);
			}
			
		  
	  }
	  
	  /**
	   * Selecteaza toate datele din baza de date corespunzatoare tipului T
	   * @return O lista ce contine toate elementele returnate
	   */
	  public List<T> selectAll(){
		  
		  StringBuilder sb = new StringBuilder();
		  sb.append("SELECT * FROM `"+ type.getSimpleName()+"`");
		  
		  String query = sb.toString();
		  Connection connection = ConnectionFactory.getConnection();
		  PreparedStatement statement = null;
		  ResultSet  result = null;
		try {	 
			  statement = connection.prepareStatement(query);
			  result = statement.executeQuery();
			  
			  return createObjects(result);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(statement);;
			ConnectionFactory.close(connection);
		}
		  
		  return null;
		
	  }
	 
	  
}

