package presentation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import bll.ClientBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.OrderItem;
import model.Product;

public class PdfGenerator {

	/**
	 * Creeaza header-urile pentru tabel.
	 * In functie de clasa se scriu atributele corespunzatoare
	 * @param name - numele clasei pentru care se face tabelul
	 * @param table - tabelul in care sa se includa header-urile
	 * @param size - numarul de coloane pe care sa-l contina tabelul
	 */
	public void addTableHeader(String name, PdfPTable table, int size) {
		
		ArrayList<String> headers = new ArrayList<String>();
		if(name.equals("client")) {
			for(Field field: Client.class.getDeclaredFields()) {
				headers.add(field.getName());
			}
		}
		else if(name.equals("product")) {
			for(Field field: Product.class.getDeclaredFields()) {
				headers.add(field.getName());
			}
		}
		else if(name.equals("order")) {
			for(Field field: Order.class.getDeclaredFields()) {
				headers.add(field.getName());
			}
		}
		else if(name.equals("orderItem")) {
			for(Field field: OrderItem.class.getDeclaredFields()) {
				headers.add(field.getName());
			}
		}
		
		if(size == 3) {
			 Stream.of(headers.get(0),headers.get(1),headers.get(2)).forEach(columnTitle ->{
				 PdfPCell header = new PdfPCell();
			        header.setBackgroundColor(BaseColor.CYAN);
			        header.setBorderWidth(1);
			        header.setPhrase(new Phrase(columnTitle));
			        header.setHorizontalAlignment(Element.ALIGN_CENTER);
			        table.addCell(header);
			    });
			}
		if(size == 4) {
			 Stream.of(headers.get(0),headers.get(1),headers.get(2),headers.get(3)).forEach(columnTitle ->{
				 PdfPCell header = new PdfPCell();
			        header.setBackgroundColor(BaseColor.CYAN);
			        header.setBorderWidth(1);
			        header.setPhrase(new Phrase(columnTitle));
			        header.setHorizontalAlignment(Element.ALIGN_CENTER);
			        table.addCell(header);
			    });
			}
		
		
	}
	
	/**
	 * Introduce randuri in tabelul creat 
	 * @param <T> - se foloseste tehnica "reflection" pentru ca metoda sa fie folosita de toate clasele
	 * @param table - refernat la tabel
	 * @param size - numarul de coloane din tabel
	 * @param content - lista de elemente pe care le vom introduce in tabel
	 */
	public <T>void addRows( PdfPTable table, int size, List<T> content) {
	
		T obj ;
	
		for(int i =0;i<content.size();i++) {
		 
				obj = content.get(i);
				for(Field field : content.get(i).getClass().getDeclaredFields()) {
					 field.setAccessible(true);
				 
				 try {
					Object value = field.get(obj);
					 PdfPCell header = new PdfPCell();
					 header.setPhrase(new Phrase(value.toString()));
				     header.setHorizontalAlignment(Element.ALIGN_CENTER);
					 table.addCell(header);
				   //table.addCell(value.toString());
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
				}
				
			}
		
	}
	
	
	 /**
	  * Creeaza un pdf in care se scrie tabelul 
	  * @param <T> - se foloseste tehnica "reflection" pentru ca metoda sa fie folosita de toate clasele
	  * @param name - numele clasei pentru care se face fisierul, folosit pentru denumirea lui
	  * @param size - numarul de coloane pe care sa-l aiba tabelul
	  * @param content - elemente cu care este populat tabelul
	  * @param nr - un numar folosit la denumirea fisierului
	  */
	public <T> void createTable(String name, int size, List<T> content, int nr) {
		
		Document document = new Document();
		StringBuilder sb = new StringBuilder();
		sb.append(name + " table"+ nr+ ".pdf");
	
		try {
			
			PdfWriter.getInstance(document, new FileOutputStream(sb.toString()));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		document.open();
		
		PdfPTable table = new PdfPTable(size);
		addTableHeader(name,table, size);
		addRows(table, size, content);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
	}
	
	/**
	 * Creeaza un fisier in format pdf ce contine mesajele de warning in cazul in 
	 * care ceva nu a mers bine in timpul comenzii de ORDER
	 * @param name - numele clientului care a facut comanda
	 * @param mesaj - mesajul ce va fi scris in fisier
	 */
	
	public void createWarningPdf( String name , String mesaj) {
		Document document = new Document();
		StringBuilder sb = new StringBuilder();
		sb.append("Warning Client " + name + ".pdf");
		try {
			PdfWriter.getInstance(document, new FileOutputStream(sb.toString()));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			
			e.printStackTrace();
		}
		document.open();
		Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		Chunk chunk = new Chunk(mesaj, font);
		try {
			document.add(chunk);
		} catch (DocumentException e) {
			
			e.printStackTrace();
		}
		document.close();
		
	}
	
	/**
	 * Creeaza o chitanta pentru o comanda. Contine numarul comenzii , numele clientului , precum si 
	 * numele produselor, canitatea acestora si pretul pentru fiecare produs, precum si cel total
	 * @param order - Obiectul de tip Order, ce contine datele despre comanda
	 * @param orderItem - O lisat de obiecte de tip OrderItem corespunzatoare id-ului comenzii. Contine
	 * date despre canrtitatea fiecarui produs cumparat
	 * @param client - obiectul de tip ClientBLL
	 * @param product - pbiectul de tip ProductBLL
	 */
	public void createBill( Order order , List<OrderItem> orderItem, ClientBLL client, ProductBLL product) {
		
		Document document = new Document();
		StringBuilder sb = new StringBuilder();
		StringBuilder nr = new StringBuilder();
		StringBuilder name = new StringBuilder();
		StringBuilder produs = new StringBuilder();
		StringBuilder total = new StringBuilder();

		sb.append("Bill number " + order.getIdOrder() + ".pdf");
		try {
			PdfWriter.getInstance(document, new FileOutputStream(sb.toString()));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		document.open();
		
		nr.append("Order nr. " + order.getIdClient() + "\n");
		Client c = client.selectById(order.getIdClient());
		name.append("Client " + c.getName() + "\n\n");
		Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		Chunk chunk = new Chunk("Bill\n", font);
		Chunk chunk1 = new Chunk(nr.toString(), font);
		Chunk chunk2= new Chunk(name.toString(), font);
		
		Paragraph par = new Paragraph();
		
			par.add(chunk);
			par.add(chunk1);
			par.add(chunk2);
		
		for(OrderItem ordItem : orderItem) {
			produs.append(ordItem.getQuantity() + " X " +ordItem.getNameProduct());
			Product p = product.selectByName(ordItem.getNameProduct());
			
			produs.append("..................." + ordItem.getQuantity()*p.getPrice() + "  lei\n");
		    Chunk chunk4 = new Chunk(produs.toString());
	        par.add(chunk4);
	        produs = new StringBuilder();
	      }
		total.append("\nTotal: " + order.getTotal() + "  lei");
		par.add(new Chunk(total.toString(), font));
	
		try {
			document.add(par);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   document.close();
	
	}
}
