package presentation;

import java.io.File;
import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;


import bll.ClientBLL;
import bll.OrderBLL;
import bll.OrderItemBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.OrderItem;
import model.Product;

public class FileParser {
 
	private File file;
	private Scanner scan;
	private List<ArrayList<String>> lines = new ArrayList<ArrayList<String>>();
	protected ClientBLL client;
	private OrderBLL order;
	private ProductBLL product;
	private OrderItemBLL orderItem;
	public static int nr, nr1, nr2 = 0;
	
	/**
	 * Constructorul clasei FileParser 
	 * @param file - fisierul din care se citeste
	 * @param c - obiectul de tip ClientBLL, folosit pentru operatiile pe baza de date
	 * @param o - obiectul de tip OrderBLL, folosit pentru operatiile pe baza de date
	 * @param i -  obiectul de tip OrderItemBLL, folosit pentru operatiile pe baza de date
	 * @param p -  obiectul de tip ClientBLL, folosit pentru operatiile pe baza de date
	 */
	public FileParser(String file, ClientBLL c, OrderBLL o, OrderItemBLL i, ProductBLL p) {
		this.file = new File(file).getAbsoluteFile();
		try {
			scan = new Scanner(this.file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.client = c;
		this.order = o;
		this.orderItem = i;
		this.product = p;	
	}
	
	/**
	 * Citeste din fisier linie cu linie, apoi cuvant cu cuvant
	 * @return O lista de linii , fiecare linie fiind o lista de cuvinte
	 */
	public List<ArrayList<String>> readCommands() {
		ArrayList<String> words;
		while(scan.hasNextLine()) {
			words = new ArrayList<String>();
			String x1 = scan.nextLine();
			final Pattern p = Pattern.compile("\\s*(\\s|:|,)\\s*");
			final String[] items = p.split(x1);
			for(final String s: items) {
				words.add(s);
				
			}
			
			lines.add(words);
		}
		
		return lines;	
	}
	
	/**
	 * Insereaza un client in baza de date, apeland metoda de insertClient din clasa ClientBLL
	 * Datele clientului se iau din lista "line"
	 * @param line - lista de cuvinte din linia corespunzatoare comenzii de insert client
	 */
	public void insertClientCommand(ArrayList<String> line) {
		Client c;
		StringBuilder name = new StringBuilder();
		name.append(line.get(2) + " " + line.get(3));
		c = new Client(name.toString(), line.get(4));
		if(client.selectByName(name.toString()) == null) {
			client.insertClient(c);
		}
	}
	
	/**
	 * Insereaza un produs in baza de date, apeland metoda insertProduct din clasa ProductBLL
	 * Datele produsului sunt extrase din lista "line"
	 * @param line - lista de cuvinte din linia corespunzatoare comenzii de insert product
	 */
	public void insertProductCommand(ArrayList<String> line) {
		Product p;
		float price= Float.parseFloat(line.get(4));
		int quantity = Integer.parseInt(line.get(3));
		p = new Product(line.get(2), quantity, price);
		
		if(product.selectByName(line.get(2)) == null) {
			product.insertProduct(p);
		}
		else {
			//produsul se gaseste, update pe pret si cantitate
			product.updateProduct(p, "price");
			product.updateProduct(p, "quantity");
			
		}
	}
	
	/**
	 * Sterge un client din baza de date, apeland metoda "selectByName" pentru extragerea Clientului
	 * din baza de date, si "deleteClient" pentru stergerea lui 
	 * @param line - lista de cuvinte din linia corespunzatoare comenzii de delete client
	 */
	public void deleteClientCommand(ArrayList<String> line) {
		Client c ;
		StringBuilder name = new StringBuilder();
		name.append(line.get(2) + " " + line.get(3));
		//c = new Client(name.toString(), line.get(4));
		c = client.selectByName(name.toString());
		if(c == null) {
			System.out.println("The client is not in our database");
			
		}
		else {
			client.deleteClient(c);
		}
	}
	/**
	 * Sterge un client din baza de date, apeland metoda "selectByName" pentru extragerea produsului
	 * din baza de date, si "deleteProduct" pentru stergerea lui 
	 * @param line - lista de cuvinte din linia corespunzatoare comenzii de delete product
	 */
	public void deleteProductCommand(ArrayList<String> line) {
		
		Product p;
		
		p = product.selectByName(line.get(2));
		if(p == null) {
			System.out.println("This product is not available");
		}
		else {
			product.deleteProduct(p);
		}
		
	}
	
	/**
	 * Se introduc datele corespunzatoare comenzii date in baza de date, atat in order 
	 * cat si in orderItem. Se verifica mai intai existenta clientului , mai apoi cea a produsului.
	 * In cazul in care nu exista se genereaza un pdf cu mesajul erorii.
	 * Daca se comanda mai multe produse decat sunt pe stoc atunci se genereaza
	 * un pdf cu un mesaj corespunzator.
	 * Daca nu exista probleme, se genereaza o chitanta pentru client
	 * @param line - lista de cuvinte din linia corespunzatoare comenzii de order
	 */
	public void orderCommand(ArrayList<String> line) {
		PdfGenerator g = new PdfGenerator();
		StringBuilder name = new StringBuilder();
		name.append(line.get(1) + " " + line.get(2));
		Client c = client.selectByName(name.toString());
		Product p;
		Order o = null;
		OrderItem oI;
		int quantity =Integer.parseInt(line.get(4));
		
		float total = 0;
		if(c == null) {
		
			g.createWarningPdf(name.toString(), "Sorry, the client is not in our database" );
			return;
		}
		else {
		
			p = product.selectByName(line.get(3));
			if(p == null) {
			
				g.createWarningPdf(name.toString(),"Sorry , the product is not available!");
				return;
			}
			else if(quantity > p.getQuantity()) {
				
				g.createWarningPdf(name.toString(), "Sorry,there are not enough products");
				return;
			}
			else {
				
				p.setQuantity(p.getQuantity() - quantity);
				product.updateProduct(p, "quantity");
				total = (float)(quantity * p.getPrice());
				o = order.selectByClientId(c.getIdClient());
				
				if(o == null) {
					// create a new order
					o = new Order(c.getIdClient(),total);
					order.insertOrder(o);
				
				}else {
					
					o.setTotal(total+o.getTotal());
					order.updateOrder(o, "total");
				}
				
				oI = orderItem.selectByIdPlusClient(o.getIdOrder(), p.getName());
				if(oI == null) {
					// create a new orderItem
					oI = new OrderItem(o.getIdOrder(),p.getName(), quantity);
					orderItem.insertOrderItem(oI);
				
				}else {
					
					oI.setQuantity(oI.getQuantity() + quantity);
					orderItem.updateOrderItem(oI, "quantity");
				}
				
			}
				
		}
		List<OrderItem> listItems = orderItem.selectById(o.getIdOrder());
		g.createBill(o, listItems, client, product);
	}
	
	/**
	 * Se genereaza pdf-ul ce contine un tabel cu clientii din baza de date, 
	 * de la momentul respectiv. Fiecare pdf va avea la final un numar, 
	 * ce reprezinta al catlea raport este ( incepand de la 0).
	 */
	public void createReportClient() {
	
		List<Client> clients = client.selectClients();
		PdfGenerator g = new PdfGenerator();
		g.createTable("client", 3, clients, nr++);
	}
	/**
	 * Se genereaza pdf-ul ce contine un tabel cu produsele din baza de date, 
	 * de la momentul respectiv. Fiecare pdf va avea la final un numar, 
	 * ce reprezinta al catelea raport este ( incepand de la 0).
	 */
	public void createReportProduct() {
		
		List<Product> products = product.selectProducts();
		PdfGenerator g = new PdfGenerator();
		g.createTable("product", 4, products, nr1++);
	}
	/**
	 * Se genereaza pdf-ul ce contine un tabel cu comenzile din baza de date, 
	 * de la momentul respectiv. Fiecare pdf va avea la final un numar, 
	 */
	public void createReportOrder() {
		
		List<Order> orders = order.selectOrders();
		PdfGenerator g = new PdfGenerator();
		g.createTable("order", 3, orders, nr2++);
		
	}
	
	/**
	 * Se determina pentru ficare linie citita din fisierul cu comenzi ce operatie
	 * sa se execute.
	 */
	public void executeCommands() {
		
		for(ArrayList<String> l : lines) {
			
			for(int index = 0;index<l.size();index++) {
				  if(l.get(index).equals("Insert")) {
					
					if(l.get(index+1).equals("client")) {
						
						insertClientCommand(l);
					}
					else if(l.get(index+1).equals("product")) {
						insertProductCommand(l);
					}
					
				}
				else if(l.get(index).equals("Delete")) {
					
					if(l.get(index+1).equals("client")) {
						
						deleteClientCommand(l);
					}
					else if(l.get(index+1).equals("product")) {
						deleteProductCommand(l);
					}
					
				}
				else if(l.get(index).equals("Report")) {
					
					if(l.get(index+1).equals("client")) {
						createReportClient();
					}
					else if(l.get(index+1).equals("product")) {
						createReportProduct();
						
					}else if(l.get(index+1).equals("order")) {
						createReportOrder();
					}
				}
				else if(l.get(index).equals("Order")) {
					orderCommand(l);
				}
					
			}
			
		}
	}
	
}
