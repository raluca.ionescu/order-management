package model;

public class OrderItem {
	
	private int idOrder;
	private String nameProduct;
	private int quantity;
	
	public OrderItem() {
		
	}
	
	public OrderItem(int idOrder, String nameProduct, int q) {
		this.idOrder = idOrder;
		this.nameProduct = nameProduct;
		this.quantity = q;
	}

	/**
	 * Construcorul clasei OrderItem . IdOrder initializat cu 0, fiind generat automat
	 * @param nameProduct - numele produsului cumparat 
	 * @param q - cantitatea cumparata
	 */
	public OrderItem(String nameProduct, int q) {
		this.idOrder = 0;
		this.nameProduct = nameProduct;
		this.quantity = q;
	}
	
	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String toString() {
		return idOrder + " " + nameProduct + " "+ quantity;
	}
}
