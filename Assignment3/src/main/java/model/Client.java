package model;

public class Client {

	 private int idClient;
	 private String name;
	 private String address;
	 
	

	public Client() {
		 
	 }
	

	 public Client(int id, String name, String address) {
		 this.idClient = id;
		 this.address = address;
		 this.name = name;
	 }
	 
		/**
		 * Constructorul clase Client. Id-ul se initializeaza cu 0, fiind generat automat
		 * @param name - numele clientului
		 * @param address- adresa clientului 
		 */
		
	 public Client(String name, String address) {
		 this.idClient = 0;
		 this.address = address;
		 this.name = name;
	 }

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String toString() {
		return idClient + " " + name + "  " + address;
	}
	

}
