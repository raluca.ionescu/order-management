package model;

public class Product {

   private int idProduct;
   private String name;
   private float price;
   private int quantity;
   
   public  Product() {
	   
   }
   
   public Product(int id, String name, int quatity, int price) {
	   this.idProduct = id;
	   this.name = name;
	   this.price = price;
	   this.quantity = quatity;
   }
/**
 * Constructorul clasei Product. idProduct initializat cu 0, fiind generat automat
 * @param name - numele produsului
 * @param quatity - stocul 
 * @param price - pretul per produs 
 */
   public Product( String name, int quatity, float price) {
	   this.idProduct = 0;
	   this.name = name;
	   this.price = price;
	   this.quantity = quatity;
   }
   
   public int getQuantity() {
	return quantity;
   }

   public void setQuantity(int quantity) {
	this.quantity = quantity;
   }

   public int getIdProduct() {
	return idProduct;
   }

   public void setIdProduct(int idProduct) {
	this.idProduct = idProduct;
   }

   public String getName() {
	return name;
   }

   public void setName(String name) {
	this.name = name;
   }

   public float getPrice() {
	return price;
   }

	public void setPrice(float price) {
		this.price = price;
	}
		
	public String toString() {
		return idProduct +  " " + name + " " + quantity + " " + price;
	}
	}
