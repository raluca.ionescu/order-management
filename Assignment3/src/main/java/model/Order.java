package model;

public class Order {

	private int idOrder;
	private int idClient;
	private float total;
	
	public Order() {
		
	}
	
	public Order(int idOrder,int idClient, float total) {
		this.idClient = idClient;
		this.idOrder = idOrder;
		this.total = total;
	}

	/**
	 * Constructorul clasei Order. IdOrder se initializeaza cu 0, fiind generat automat
	 * @param idClient - id-ul clientului
	 * @param total  - totalul comenzilor clientului
	 */
	public Order(int idClient, float total) {
		this.idClient = idClient;
		this.idOrder = 0;
		this.total = total;
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}
	public String toString() {
		return idOrder + " "+ idClient + " " + total;
	}
}
